# removing folder
  #Type: rm dir rails_projects

# removing folder with files in it
  #Type: rm -rf ruby_projects

# installing new ruby version
  #Type: rvm install ruby-2.3.4

# prints to the screen but does not <br> after
print "Hello World"

# prints to the screen buts a <br> after
puts "Hello World"

#methods
# .upcase, .lowercase, .reverse

name = "Stebs".upcase.reverse

puts name