cities = {
    'San Francisco' => '415',
    'Concord' => '925',
    'Daly City' => '650',
    'Ohio' => '216'   
  }

#Method to print out the value for each "key"
def get_city_names(city)
  city.each { |k, v| puts k }
end

#Method to get area code
#somehash = cities
#key = 'San Francisco', 'Concord, 'Daly City', 
#or Ohio
def get_area_code(somehash, key)
  somehash[key]
end

# "Loop do" allows multiple prompts to be asked
# The loop will continue unless you press ctrl + c 
# or if you insert a break
loop do
 
puts "Do you want to look up an area code based on
a city name? (Y/N)"
answer = gets.chomp

#If user enters anything but "Y", end the loop
if answer != "Y"
  break
end
  
puts "Which city do you want the area code for?"
#Displays the city names
get_city_names(cities)
city = gets.chomp

#if the user types in a valid city from the list
if cities.include?(city)
  puts "The area code for #{city} is #{get_area_code(cities, city)}"
  
else
#if not user does not type a valid city
  puts "You entered an invalid city"
end
  
end
