# prints to the screen but does not <br> after
print "Print Hello World"

# prints to the screen buts a <br> after
puts "Puts Hello World"



#predefined methods
# .upcase, .lowcase, .reverse, length

name = "Stebs".upcase.reverse

nick_name ="Steboogie".length 

puts name # displays "SBETS"
puts nick_name #displays 9



# methods = ruby, functions = javascript

#Example 1 (creating a method):
def say_hello
  puts "Method Hello World"
end

say_hello


#Example 2 (using an arugment):
def say_hello(anything)
  puts anything 
end

say_hello "This is using an argument"
say_hello "I can print anything"


# Writing ruby in terminal
  #Type: irb

# *****Finding out type of:*****

last_name = "Gangano"
num = 1

#Displays "string"
puts last_name.class
#Displays "string"
puts num.class



#To find out all the methods that can be used 
# with a variable, type "last_name.methods"

# *Changing the value of a variable*
first_name = "Darren"
alter_ego = "Dfunk"

first_name = alter_ego

#Displays "Dfunk"
puts alter_ego



# *String concatination*
puts "Steven" + " " + "Gangano"



# *String interpelation*
work = "Amtrust North America"
my_job = "I work at #{work}!"

#Displays: 
puts my_job



#Using prompts
puts "Hello enter your first name"
first_name = gets.chomp

puts "What is your last name?"
last_name = gets.chomp

puts "Welcome #{first_name} #{last_name}, 
to the playground!"



















