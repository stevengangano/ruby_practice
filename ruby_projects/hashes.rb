#Hashes (similar to object key value pairs in javascript)
my_details = {'name' => 'Stebs', 'favcolor' => 'red'}

#Displays: Stebs
puts my_details["name"]
myhash = {a: 1, b: 2, c: 3}
#Displays: 2
puts myhash[:b]


#Adding a new value to myhash
myhash[:d] = "Steboogie"
#Displays: Steboogie
puts myhash[:d]

#Deleting a hash
myhash.delete(:b)
puts myhash

#Looping through each key:value pair using each
numbers = {a: 1, b: 2, c: 3, d: 4, e: 5}
#Prints out the value for each key
numbers.each { |k, v| puts v }
#Printing out both key and value
numbers.each { |k, v| puts "The key is #{k} and the value is #{v}" }

#Removing number from the hash
numbers.each { |k, v| numbers.delete(k) if v > 2}
puts numbers

#Selecting numbers that are odd
myhash[:c] = 7
myhash[:d] = 9

numbers.select { |k, v| v.odd? }
puts myhash








