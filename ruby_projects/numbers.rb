#Using Numbers

#addition
puts 5+3

#subtraction
puts 10-4

#multiplicaton
puts 8*4

#division (# needs to use deciams of ".to_f 
# to get exact decimal number)
# .to_f = float
puts 20/3.to_f
puts 20.0/3
puts 20/3.0

#Using variables
y = 20
x = 3
puts y.to_f/x

#Using modular (gives the remainder)
puts 6 % 4

#Finding out if a number is even or odd
puts 22.odd? #displays false
puts 22.even? #displays true

#Generating a random number

#Generates a random number between 0-1
puts rand

#Generates a random number between 0-10 but it 
#excludes 10
puts rand(10)


#Turning a string to an integer
y = "12"
puts y.to_i


#Turning an integer into a string
x = 25
puts x.to_s * 25

#Repeating
5.times {puts "Stebs"}

#Using Math in Prompts
puts "Please enter your first number"
first_number = gets.chomp #this is a string

puts "Please enter your second number"
second_number = gets.chomp #this is a string

puts "The first number multiplied by the 
second number is: #{first_number.to_i * 
second_number.to_i}"















