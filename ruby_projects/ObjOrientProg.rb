module Destructable
  def destroy(anyobj)
    puts "This is destroyed"
  end
end

class User
  
  #Allows module Destructable to be used
  include Destructable
  
  #This creates both setter and getter method
  #No need to create get_name and change_name
  attr_accessor :name, :email
  
  #Method to give user a name
  def initialize(name, email)
    @name = name
    @email = email
  end
  
  #Method to get name. 
  #Getter Method
  #This is an instance method
  def get_name
    @name
  end

  #Method to change the name
  #Setter method
  #This is an instance method
  def change_name(name)
    @name = name
  end
  
  #This is an instance method
  def run
    puts "Hey I'm running!"
  end
  
  #This is a class Method
  #Can access without having to create a new object
  def self.identify_yourself
    puts "Hey I'm a class method"
  end

end

#Initialize an object the class User
#this creates an object user
#<User:0x000000015a4348>
user = User.new("Stebs", "steven@gmail.com")

#Displays the name
# puts user.get_name

#Changing the name
# user.change_name("Dfunk")
# puts user.get_name


#Displays the name (get_name) & first email
puts "#{user.name}, #{user.email}"
#Changes the name (change_name)
user.name = "Stebzs"
#Changes the email
user.email = "stevengangano@gmail.com"
puts "#{user.name}, #{user.email}"
#Displays "Hey I'm running"
puts user.run

#Modules

#Displays: This is destroyed
user.destroy(user)




#Inheritance

#Buyer has the same method defined in User
class Buyer < User
  def run
    puts "Hey I'm not running"
  end 
end

#Create a new user
buyer = Buyer.new("Dfunk", "soapandwater@gmail.com")
#Displays name and email
puts "#{buyer.name}, #{buyer.email}"
#Display "Hey I'm not running". Overrides User "run" method
buyer.run

#Displaying the class method
User.identify_yourself







=begin
This where User is inherited. Top to bottom.
 -User
 -Object
 -Kernel
 -BasicObject
=end
puts User.ancestors

