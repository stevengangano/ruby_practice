# https://github.com/bbatsov/ruby-style-guide
# How to write correct ruby code

a = [1, 2, "Steboogie"]

puts a[0]

#checking if it is inside the array
puts a.include?("Steboogie")

#reverses order of array
#"!" updates the original array
a.reverse!
puts a

#shuffles order of the array
a.shuffle!
puts a

#creating a RANGE of numbers into an array
#excludes 
x = (0..5).to_a
#adding an item to end of the array
x << 25
x.push("asljfl")
puts x
#adding to beginning of the array
x.unshift("Stebs")
puts x
#removing the last item from the array
x.pop
puts x

#Looping through each item in the  array
x.each {|i| puts i}

#for loop
# What is happening?
# "number" can be named anything you want
# For every item in the array "x"
# Displays "Hi". 
#
for number in x
  puts "Hi"
end


#Looping using each
#capitalize use uppercase for first character
names = ["joe", "john", "mark"]

# names.each do |name|
#   puts "Hello #{name.capitalize}"
# end

#or PREFERRED WAY

names.each { |name| puts "Hello #{name.capitalize}" }

#Join Method
myName = ["My", "name", "is", "Stebs"]

# Displays: My name is Stebs
puts myName.join(" ")







